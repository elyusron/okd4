# 1. Prerequisites:
## 1.1 Request More Quotas in Project:
- https://console.cloud.google.com/iam-admin/quotas?_ga=2.141178674.1231362403.1644543553-1312643488.1643789210
- quotas: CPUs: (N1,E2): Min 32
- quotas: N2_CPUs: (N2): Min 32
- quotas: Persisten Disk SSD: Min 1000 Gb
## 1.2 enable required API
- Compute Engine API
- Google Cloud APIs
- Cloud Resource Manager API
- Google DNS API
- IAM Service Account Credentials API
- Identity and Access Management (IAM) API
- Service Management API
- Service Usage API
- Google Cloud Storage JSON API
- Cloud Storage



## 1.3 DNS
### create public zone DNS
```
- gcloud dns managed-zones create <okd4-public-zone> --dns-name <base.domain.com.> --description ""
- gcloud dns managed-zones describe <okd4-public-zone>
```
### add NS record in existing public DNS
```
- type: NS
- name: <only-sub-domain>
- content: <all-gcp-specific-NS-from-describe-command>
```

### Validate propagation:
```
- dig @8.8.8.8 <base.domain.com> NS +short
```

## 1.4 Load Balancer
- automatic


# 2. Implementation:
## 2.1 Setup
```
- gcloud config list
- gcloud config set compute/region <asia-southeast2>
- gcloud config set project <myProjectId>
```
## 2.2 create service account as owner:
```
- gcloud iam service-accounts create <okd4-admin>
- gcloud projects add-iam-policy-binding ${gcp-project} --member "serviceAccount:${service-account-name}@${gcp-project}.iam.gserviceaccount.com" --role "roles/owner"
```

## 2.3 download json service account:
```
- mkdir ~/.gcp
- gcloud iam service-accounts keys create ~/.gcp/osServiceAccount.json --iam-account <okd4-admin@cloud-native-okd4.iam.gserviceaccount.com>
```

## 2.4 Generate ssh key pair:
```
ssh-keygen -t rsa -b 2048 -C "user@hostname-or-domain.com"
```

## 2.5 Copy pull secret from Red Hat:
- https://console.redhat.com/openshift/install/gcp/installer-provisioned
- copy pull secret to clipboard, use it later to paste while installing cluster

## 2.6 Install OKD4 by IPI:
```
- mkdir installation_directory
- ./openshift-install create cluster --dir installation_directory
```

## 2.7 Monitor progress:
```
- ./openshift-install --dir installation_directory wait-for bootstrap-complete --log-level=info
```
